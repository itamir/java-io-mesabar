package br.ufrn.imd.dominio;

public class ItemDeCardapio {

	private String descricao;
	
	private double preco;
	
	private boolean flagDisponivel;

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public double getPreco() {
		return preco;
	}

	public void setPreco(double preco) {
		this.preco = preco;
	}

	public boolean isFlagDisponivel() {
		return flagDisponivel;
	}

	public void setFlagDisponivel(boolean flagDisponivel) {
		this.flagDisponivel = flagDisponivel;
	}

	@Override
	public String toString() {
		return descricao + " R$ " + preco;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemDeCardapio other = (ItemDeCardapio) obj;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		return true;
	}
}
