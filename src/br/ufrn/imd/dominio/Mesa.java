package br.ufrn.imd.dominio;

public class Mesa {

	private int numero;

	private Consumo consumo;
	
	public Mesa(int numero) {
		this.numero = numero;
		consumo = new Consumo();
	}
	
	public void iniciarConsumo() {
		consumo.iniciarConsumo();
	}
	
	public void encerrarConsumo() {
		consumo.encerrarConsumo();
	}
	
	public int getNumero() {
		return numero;
	}

	public void setNumero(int numero) {
		this.numero = numero;
	}

	public Consumo getConsumo() {
		return consumo;
	}

	public void setConsumo(Consumo consumo) {
		this.consumo = consumo;
	}
}
