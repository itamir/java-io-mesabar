package br.ufrn.imd.dominio;

public class ItemDeConsumo {

	private ItemDeCardapio itemDeCardapio;
	
	private double quantidade;

	public ItemDeConsumo() { }
	
	public ItemDeConsumo(ItemDeCardapio itemDeCardapio,
			double quantidade) {
		this.itemDeCardapio = itemDeCardapio;
		this.quantidade = quantidade;
	}
	
	
	public double getTotalItemConsumo() {
		return itemDeCardapio.getPreco() * quantidade;
	}
	
	
	public ItemDeCardapio getItemDeCardapio() {
		return itemDeCardapio;
	}

	public void setItemDeCardapio(ItemDeCardapio itemDeCardapio) {
		this.itemDeCardapio = itemDeCardapio;
	}

	public double getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(double quantidade) {
		this.quantidade = quantidade;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((itemDeCardapio == null) ? 0 : itemDeCardapio.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemDeConsumo other = (ItemDeConsumo) obj;
		if (itemDeCardapio == null) {
			if (other.itemDeCardapio != null)
				return false;
		} else if (!itemDeCardapio.equals(other.itemDeCardapio))
			return false;
		return true;
	}
}
