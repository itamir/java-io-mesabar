package br.ufrn.imd.dominio;

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class Consumo {
	
	private boolean flagContaAberta;
	
	private Mesa mesa;
	
	private List<ItemDeConsumo> itensDeConsumo;
	
	private Date dtHoraInicioConsumo;
	
	private Date dtHoraFimConsumo;

	public void iniciarConsumo() {
		dtHoraInicioConsumo = new Date();
	}
	
	public void encerrarConsumo() {
		dtHoraFimConsumo = new Date();
	}
	
	public void adicionarItemDeCardapio(ItemDeCardapio item, 
			double qtd) {
		if(itensDeConsumo == null)
			itensDeConsumo = new ArrayList<ItemDeConsumo>();
		
		ItemDeConsumo itemConsumo = getNovoItemConsumo(item, qtd);

		if(itensDeConsumo.contains(itemConsumo)) {
			ItemDeConsumo itemAux = itensDeConsumo.
					get(itensDeConsumo.indexOf(itemConsumo));
			itemConsumo.setQuantidade(itemAux.getQuantidade() + qtd);
			itensDeConsumo.remove(itemAux);
		} 

		itensDeConsumo.add(itemConsumo);
	}

	private ItemDeConsumo getNovoItemConsumo(ItemDeCardapio item, double qtd) {
		ItemDeConsumo itemConsumo = new ItemDeConsumo();
		itemConsumo.setItemDeCardapio(item);
		itemConsumo.setQuantidade(qtd);
		return itemConsumo;
	}
	
	public void retirarItemDeCardapio(ItemDeCardapio item,double qtd){
		if(itensDeConsumo != null) {
			
			ItemDeConsumo itemConsumo = getNovoItemConsumo(item, qtd);
			
			ItemDeConsumo itemAux = itensDeConsumo.
					get(itensDeConsumo.indexOf(itemConsumo));
			
			itemConsumo.setQuantidade(itemAux.getQuantidade() - qtd);
			itensDeConsumo.remove(itemAux);
			
			if(itemConsumo.getQuantidade() > 0)
				itensDeConsumo.add(itemConsumo);
		}
	}
	
	public String emiteFatura() {
		String resultado = "Conta da Mesa " + mesa.getNumero() + "\n";
		int count = 0;
		if(itensDeConsumo != null) {
			for (ItemDeConsumo itemDeConsumo : itensDeConsumo) {
				resultado += (++count) + " - " + 
			    itemDeConsumo.getItemDeCardapio().getDescricao() + " "
			    + itemDeConsumo.getQuantidade() + " R$ " 
			    + itemDeConsumo.getItemDeCardapio().getPreco()+ " R$ "
			    + itemDeConsumo.getTotalItemConsumo() + "\n";
			}
			resultado += "TOTAL: R$" + getTotalConta();
		}
		return resultado;
	}
	
	public void imprimeFatura() throws IOException {
		String fatura = emiteFatura();
		OutputStream os = new FileOutputStream("fatura.txt");
		OutputStreamWriter osw = new OutputStreamWriter(os);
		BufferedWriter bw = new BufferedWriter(osw);
		bw.write(fatura);
		bw.close();
	}
	
	public double getTotalConta() {
		double total = 0;
		if(itensDeConsumo != null) {
			for (ItemDeConsumo itemDeConsumo : itensDeConsumo) {
				total += itemDeConsumo.getTotalItemConsumo();
			}
		}
		return total * 1.1;
	}
	
	
	public boolean isFlagContaAberta() {
		return flagContaAberta;
	}

	public void setFlagContaAberta(boolean flagContaAberta) {
		this.flagContaAberta = flagContaAberta;
	}

	public Mesa getMesa() {
		return mesa;
	}

	public void setMesa(Mesa mesa) {
		this.mesa = mesa;
	}

	public List<ItemDeConsumo> getItensDeConsumo() {
		return itensDeConsumo;
	}

	public void setItensDeConsumo(List<ItemDeConsumo> itensDeConsumo) {
		this.itensDeConsumo = itensDeConsumo;
	}

	public Date getDtHoraInicioConsumo() {
		return dtHoraInicioConsumo;
	}

	public void setDtHoraInicioConsumo(Date dtHoraInicioConsumo) {
		this.dtHoraInicioConsumo = dtHoraInicioConsumo;
	}

	public Date getDtHoraFimConsumo() {
		return dtHoraFimConsumo;
	}

	public void setDtHoraFimConsumo(Date dtHoraFimConsumo) {
		this.dtHoraFimConsumo = dtHoraFimConsumo;
	}
}
