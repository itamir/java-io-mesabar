package br.ufrn.imd.main;

import java.io.IOException;

import br.ufrn.imd.dominio.ItemDeCardapio;
import br.ufrn.imd.dominio.Mesa;

public class Execucao {

	public static void main(String[] args) throws IOException {
		
		//itens de card�pio
		ItemDeCardapio cerveja = new ItemDeCardapio();
		cerveja.setDescricao("Cerveja SKOL");
		cerveja.setPreco(5);
		cerveja.setFlagDisponivel(true);

		ItemDeCardapio espeto = new ItemDeCardapio();
		espeto.setDescricao("Espeto de carne");
		espeto.setPreco(2);
		espeto.setFlagDisponivel(true);
		
		ItemDeCardapio fileFritas = new ItemDeCardapio();
		fileFritas.setDescricao("Fil� com fritas");
		fileFritas.setPreco(25);
		fileFritas.setFlagDisponivel(true);

		Mesa mesa1 = new Mesa(1);
		mesa1.getConsumo().setMesa(mesa1);
		Mesa mesa2 = new Mesa(2);
		mesa2.getConsumo().setMesa(mesa2);
		
		//chegaram pessoas nas mesas
		mesa1.iniciarConsumo();
		mesa2.iniciarConsumo();
		
		//mesa1 pediu fil� com fritas e cerveja
		mesa1.getConsumo().adicionarItemDeCardapio(fileFritas, 1);
		mesa1.getConsumo().adicionarItemDeCardapio(cerveja, 3);
		
		System.out.println(mesa1.getConsumo().emiteFatura());
		
		//mesa2 pediu espetos e cervejas
		mesa2.getConsumo().adicionarItemDeCardapio(espeto, 5);
		mesa2.getConsumo().adicionarItemDeCardapio(cerveja, 2);
		
		System.out.println(mesa2.getConsumo().emiteFatura());
		
		mesa2.getConsumo().retirarItemDeCardapio(cerveja, 1);
		
		System.out.println(mesa2.getConsumo().emiteFatura());
		
		mesa1.getConsumo().imprimeFatura();
		
	}

}
